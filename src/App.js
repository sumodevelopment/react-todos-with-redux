import './App.css';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Navbar from "./features/Navbar/Navbar";
import TodoList from "./features/Todo/TodoList";
import TodoDetail from "./features/Todo/TodoDetail";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Navbar />
                <Switch>
                    <Route path="/" exact>
                        <Redirect to="/todos"/>
                    </Route>
                    <Route path="/todos" exact component={TodoList}/>
                    <Route path="/todos/:id" component={TodoDetail}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
