import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {todoActionFetch} from "./state/todoActions";
import AppContainer from "../../hoc/AppContainer";
import TodoListItem from "./TodoListItem";
import styles from './TodoList.module.css'
import {useHistory} from "react-router-dom";

const TodoList = () => {

    const {todos, error, isLoading} = useSelector(state => state.todo)
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {
        dispatch(todoActionFetch())
    }, [])

    const handleTodoClick = todo => {
        history.push(`/todos/${todo.id}`)
    }

    return (
        <AppContainer>
            <ul className={styles.TodoList}>
                {
                    todos.map(todo => (
                        <TodoListItem key={todo.id} todo={todo} clicked={handleTodoClick}/>
                    ))
                }
            </ul>
        </AppContainer>
    )
}
export default TodoList