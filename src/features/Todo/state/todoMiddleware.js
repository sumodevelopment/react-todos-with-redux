import {ACTION_TODO_FETCH, ACTION_TODO_SET_ERROR, todoActionError, todoActionSet} from "./todoActions";

export const todoMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_TODO_FETCH) {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(r => r.json())
            .then(todos => {
                dispatch(todoActionSet(todos))
            })
            .catch(error => {
                dispatch(todoActionError(error.message))
            })
    }

    if (action.type === ACTION_TODO_SET_ERROR) {
        // Trigger a async log.
    }


}