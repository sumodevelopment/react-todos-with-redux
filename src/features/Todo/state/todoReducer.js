import {ACTION_TODO_FETCH, ACTION_TODO_SET, ACTION_TODO_SET_ERROR} from "./todoActions";

const initialState = {
    isLoading: true,
    todos: [],
    error: ''
}

export const todoReducer = (state = initialState, action) => {

    switch (action.type) {

        case ACTION_TODO_FETCH:
            return {
                ...state,
                isLoading: true,
                error: ''
            }

        case ACTION_TODO_SET:
            return {
                isLoading: false,
                error: '',
                todos: action.payload
            }

        case ACTION_TODO_SET_ERROR:
            return {
                ...state,
                isLoading: false,
                error: action.payload
            }

        default:
            return state

    }

}