import {useEffect} from "react";

const User = ({ userId }) => { // Requires a prop - userId

    useEffect(() => {
        // Fetch the user detail - dispatch action with userId
    }, [])

    return (
        <h4>User</h4>
    )
}

export default User
